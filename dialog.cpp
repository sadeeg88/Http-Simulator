#include "dialog.h"
#include "ui_dialog.h"
#include <QtWebKit/QWebView>
#include <QTime>
#include <QtNetwork/qnetworkproxy.h>
#include <QFile>
#include <QWaitCondition>
#include <QTimer>
#include <QWebFrame>
#include <QThread>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    bStop = false;
    iErrorCout = 0;
    connect(ui->btnStart,SIGNAL(clicked()),this,SLOT(start()));
    connect(ui->btnStop,SIGNAL(clicked()),this,SLOT(stop()));
       ui->btnStop->setDisabled(true);
     ui->webView->page()->networkAccessManager()->setProxy(QNetworkProxy(QNetworkProxy::HttpProxy,"192.168.1.10",80));
     ui->webView->page()->settings()->setObjectCacheCapacities(0, 0, 0);
     timer = new QTimer(this);

}

Dialog::~Dialog()
{
    delete ui;
    delete timer;
}

void Dialog::LoadNext(bool ok)
{

    QString strTime = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ");
    disconnect(ui->webView,SIGNAL(loadFinished(bool)),this,SLOT(LoadNext(bool)));
    if (!ok)
    {
        ui->textEdit->setText(strTime+"Fehler beim Laden von"+URLs.at(iPos)+"\r\n"+ui->textEdit->toPlainText());
        iErrorCout++;
        QRect WebView1 = ui->webView->geometry();
        delete ui->webView;
        ui->webView = new QWebView(this);
        ui->webView->setObjectName(QString::fromUtf8("webView"));
        ui->webView->setGeometry(WebView1);
        ui->webView->page()->networkAccessManager()->setProxy(QNetworkProxy(QNetworkProxy::HttpProxy,"192.168.1.10",80));
        ui->webView->page()->settings()->setObjectCacheCapacities(0, 0, 0);
        ui->lbErrorCountContent->setText(QString::number(iErrorCout,10));
        ui->webView->setUrl(QUrl("about:blank"));
        ui->webView->show();

    }
    if(ok && (ui->webView->page()->mainFrame()->toHtml().contains("<title>FEHLER: Die angeforderte URL konnte nicht gefunden werden</title>") == true) )
    {
        ui->textEdit->setText(strTime+"Fehler beim Laden von"+URLs.at(iPos)+"\r\n"+ui->textEdit->toPlainText());
        iErrorCout++;
        ui->lbErrorCountContent->setText(QString::number(iErrorCout,10));
    }
    if(iPos + 1 < URLs.count())
        iPos++;
    else
    {
         ui->textEdit->setText(strTime+"Schlafe 10 Sekunden\r\n"+ui->textEdit->toPlainText());
        QTimer::singleShot(10000, this, SLOT(start()));
        return;
    }
    if(!bStop)
    {

    connect(ui->webView,SIGNAL(loadFinished(bool)),this,SLOT(LoadNext(bool)));
    ui->webView->setUrl(QUrl(URLs.at(iPos)));

     ui->textEdit->setText(strTime+"Lade "+URLs.at(iPos)+"\r\n"+ui->textEdit->toPlainText());
    }
    else
    {
        ui->textEdit->setText(strTime+"Verarbeitung gestoppt\r\n"+ui->textEdit->toPlainText());
        ui->btnStart->setDisabled(false);
        ui->btnStop->setDisabled(true);

    }
}

void Dialog::start()
{
    QString strTime = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ");
    if(!bStop)
    {
     readSiteConfig();
     iPos = 0;
     ui->webView->load(QUrl(URLs.at(iPos)));
      ui->textEdit->setText(strTime+"Lade "+URLs.at(iPos)+"\r\n"+ui->textEdit->toPlainText());
      connect(ui->webView,SIGNAL(loadFinished(bool)),this,SLOT(LoadNext(bool)));
      ui->btnStart->setDisabled(true);
      ui->btnStop->setDisabled(false);
    }
    else
    {
        ui->textEdit->setText(strTime+"Verarbeitung gestoppt\r\n"+ui->textEdit->toPlainText());
        ui->btnStart->setDisabled(false);
        ui->btnStop->setDisabled(true);

    }

}

void Dialog::stop()
{
    bStop = true;
}

void Dialog::readSiteConfig()
{
    QString strTime = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss ");
    URLs.clear();
    QFile file("testsites.txt.txt");
    if(file.open(QIODevice::ReadOnly))
    {
        QTextStream in(&file);
        ui->textEdit->setText(strTime+"Lese URLs auf Datei ein.\r\n"+ui->textEdit->toPlainText());
        while (!in.atEnd()) {
            QString line = in.readLine();
            URLs<<line;
        }
     }
    else
    {
      URLs << "";
       ui->textEdit->setText(strTime+file.errorString()+ui->textEdit->toPlainText());
    }
    file.close();

}
