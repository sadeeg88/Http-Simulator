#-------------------------------------------------
#
# Project created by QtCreator 2011-11-26T16:00:40
#
#-------------------------------------------------

QT       += core gui webkit network

TARGET = HttpSimulator
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h

FORMS    += dialog.ui
