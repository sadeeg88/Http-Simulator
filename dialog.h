#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
    class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;

    QStringList URLs;
    int iPos;
    int iErrorCout;
    bool bStop;
    void readSiteConfig();
    QTimer * timer;
private slots:
    void LoadNext(bool ok);
    void start();
    void stop();
};

#endif // DIALOG_H
